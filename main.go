package main

import (
	"fmt"
	"log"
	"strings"
	"sync"

	"revoke_all_android_apps_permissions/permissions"

	adb "github.com/zach-klippenstein/goadb"
)

func main() {
	adb_client, err := adb.New()
	if err != nil {
		log.Fatal(fmt.Errorf("adb.NewWithConfig: %s", err))
	}

	if err = adb_client.StartServer(); err != nil {
		log.Fatal(fmt.Errorf("adb_client.StartServer: %s", err))
	}

	serials, err := adb_client.ListDeviceSerials()
	if err != nil {
		log.Fatal(fmt.Errorf("adb_client.ListDevices: %s", err))
	}

	for _, serial := range serials {
		device_descriptor := adb.DeviceWithSerial(serial)
		device := adb_client.Device(device_descriptor)
		list_packages, err := device.RunCommand(`pm list packages | grep "com.google.android.apps"`)
		if err != nil {
			log.Fatal(fmt.Errorf("adb_client.ListDevices: %s", err))
		}

		packages := strings.Split(list_packages, "\n")

		var wg sync.WaitGroup
		for _, pkg := range packages {
			if pkg == "" {
				continue
			}
			for _, permission := range permissions.AndroidPermissions {
				wg.Add(1)
				go revokePermission(device, pkg, permission, &wg)
			}
			wg.Wait()
		}
	}
}

func revokePermission(device *adb.Device, pkg, perm string, wg *sync.WaitGroup) {
	defer wg.Done()
	command := fmt.Sprintf("pm revoke %s %s", pkg, perm)
	fmt.Println(command)
	_, err := device.RunCommand(command)
	if err != nil {
		log.Fatalf("revokePermission: %s", err)
	}
}
